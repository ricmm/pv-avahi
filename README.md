## Install

To install use pvr from docker repo:

```
pvr app add --from=registry.gitlab.com/pantacor/pv-platforms/pv-avahi pv-avahi
pvr add
pvr commit
pvr post -m "add pv-avahi"
```

## Configure

You can configure pv-avahi behaviour through user-meta of your device.

The following keys are available:

```
# enable/disable debug logging
pv-avahi/debug => yes/no

```
