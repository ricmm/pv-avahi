FROM alpine as qemu

RUN if [ -n "aarch64" ]; then \
		wget -O /qemu-aarch64-static https://github.com/multiarch/qemu-user-static/releases/download/v4.0.0-5/qemu-aarch64-static; \
	else \
		echo '#!/bin/sh\n\ntrue' > /qemu-aarch64-static; \
	fi; \
	chmod a+x /qemu-aarch64-static

FROM arm64v8/alpine:3.8

COPY --from=qemu /qemu-aarch64-static /usr/bin/

COPY zeronetworking /etc/init.d/zeronetworking

RUN apk update; apk add openrc \
		busybox-initscripts \
		busybox-extras \
		avahi; \
	rc-update add zeronetworking sysinit default; \
	rc-update add syslog sysinit boot default; \
	rc-update add avahi-daemon default; \
	rc-update add ntpd default

RUN echo "SYSLOGD_OPTS=\"\"" > /etc/conf.d/syslog

COPY interfaces /etc/network/interfaces
ADD avahi-daemon.conf /etc/avahi/
ADD avahi-daemon /etc/init.d/
ADD pv-avahid /sbin/
ADD pv-avahi /etc/init.d/

RUN rc-update add pv-avahi default

CMD [ "/sbin/init" ]
